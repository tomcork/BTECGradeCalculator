﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeCalculator.BO
{
    public class Unit
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public bool IsMandatory { get; set; }

        public Unit()
        {

        }

        public Unit(int number, string name, bool isMandatory)
        {
            Number = number;
            Name = name;
            IsMandatory = isMandatory;
        }

        public override string ToString()
        {
            string result = Name;

            if (IsMandatory)
                result = "* " + Name;

            return result;
        }
    }
}
