﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeCalculator.BO
{
    public class Grade
    {
        public int UnitNumber { get; set; }
        public string UnitName { get; set; }
        public string Mark { get; set; }

        public Grade()
        {
            UnitNumber = 0;
            UnitName = "";
            Mark = "Failed";
        }

        public Grade(int unitNumber, string unitName, string mark)
        {
            UnitNumber = unitNumber;
            UnitName = unitName;
            Mark = mark;
        }
    }
}
