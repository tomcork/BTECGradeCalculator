﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace GradeCalculator
{
    class XmlUnitInformation
    {
        public string Path { get; set; }

        public XmlUnitInformation()
        {
            Path = "Data/CourseUnits.xml";
        }

        /// <summary>
        /// Loads the units from the xml file: /Data/CourseUnits.xml
        /// </summary>
        /// <returns>Array of units read</returns>
        public BO.Unit[] Load()
        {
            List<BO.Unit> Units = new List<BO.Unit>();

            XmlDocument xmlUnits = new XmlDocument();
            xmlUnits.Load(Path);
            foreach (XmlNode node in xmlUnits.SelectNodes("Units/Unit"))
            {
                bool mandatory = false;

                if (node.SelectSingleNode("Mandatory").InnerText == "True")
                    mandatory = true;
                
                Units.Add(
                    new BO.Unit(
                        Convert.ToInt32(
                            node.SelectSingleNode("Number").InnerText),
                        node.SelectSingleNode("Name").InnerText,
                        mandatory));
            }

            return Units.ToArray();
        }
    }
}
