﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml;

namespace GradeCalculator
{
    class XmlGradeInformation
    {
        public string Path { get; set; }

        public XmlGradeInformation()
        {
            Path = "";
        }

        public void Save(BO.Grade[] grades)
        {
            if (!Path.Equals(""))
            {
                XmlTextWriter xWriter = new XmlTextWriter(Path, Encoding.UTF8);
                xWriter.Formatting = Formatting.Indented;

                xWriter.WriteStartElement("Units");     // <Units>

                foreach (BO.Grade grade in grades)
                {
                    xWriter.WriteStartElement("Unit");                  //  <Unit>
                    xWriter.WriteStartElement("Number");                //   <Number>
                    xWriter.WriteString(grade.UnitNumber.ToString());   //      grade.UnitNumber
                    xWriter.WriteEndElement();                          //   </Number>
                    xWriter.WriteStartElement("Name");                  //   <Name>
                    xWriter.WriteString(grade.UnitName);                //      grade.UnitName
                    xWriter.WriteEndElement();                          //   </Name>
                    xWriter.WriteStartElement("Grade");                 //   <Grade>
                    xWriter.WriteString(grade.Mark);                    //      grade.Grade
                    xWriter.WriteEndElement();                          //   </Grade>
                    xWriter.WriteEndElement();                          //  </Unit>
                }

                xWriter.WriteEndElement(); // </Units>
                xWriter.Close();
            }
        }

        public BO.Grade[] LoadGradesArray()
        {
            List<BO.Grade> grades = new List<BO.Grade>();

            XmlDocument xmlGrades = new XmlDocument();
            try
            {
                xmlGrades.Load(Path);

                foreach (XmlNode node in xmlGrades.SelectNodes("Units/Unit"))
                {
                    grades.Add(
                        new BO.Grade(
                            Convert.ToInt32(
                                node.SelectSingleNode("Number").InnerText),
                            node.SelectSingleNode("Name").InnerText,
                            node.SelectSingleNode("Grade").InnerText));
                }

                return grades.ToArray();
            }
            catch
            {
                //no file was selected.
                return null;
            }

        }

        //This checks for matches to so that if the user enters
        //a value not found in the units xml file, the program
        //does not crash.
        public void Load(
            ComboBox[] UnitComboBoxes, 
            ComboBox[] GradeComboBoxes,
            List<BO.Unit> unitsWithMandatory,
            List<BO.Unit> unitsWithoutMandatory)
        {
            BO.Grade[] grades = LoadGradesArray();

            if (grades != null)
            {
                for (int i = 0; i < grades.Count(); i++)
                {
                    bool mandatory = false;
                    if (grades[i].UnitNumber == 1 ||
                        grades[i].UnitNumber == 2 ||
                        grades[i].UnitNumber == 3 ||
                        grades[i].UnitNumber == 6 ||
                        grades[i].UnitNumber == 11 ||
                        grades[i].UnitNumber == 14)
                    {
                        mandatory = true;
                    }

                    BO.Unit unit = new BO.Unit(
                        grades[i].UnitNumber,
                        grades[i].UnitName,
                        mandatory);

                    // Used to know if the item is found 
                    // in order to break out of the loop.
                    bool matchFound = false;
                    if (unit.IsMandatory)
                    {
                        foreach (BO.Unit lUnit in unitsWithMandatory)
                        {
                            if (!matchFound && lUnit.Name == unit.Name)
                            {
                                unit = lUnit;
                                matchFound = true;
                            }

                            if (matchFound)
                                break;
                        }
                    }

                    if (!matchFound)
                    {
                        foreach (BO.Unit lUnit in unitsWithoutMandatory)
                        {
                            //Check matchFound here so it doesnt check the
                            //rest of the items if it has already found it
                            if (!matchFound && lUnit.Name == unit.Name)
                            {
                                unit = lUnit;
                                matchFound = true;
                            }

                            if (matchFound)
                                break;
                        }
                    }

                    UnitComboBoxes[i].SelectedItem = unit;
                    GradeComboBoxes[i].Text = grades[i].Mark;
                }
            }
        }
    }
}
