﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace GradeCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<BO.Unit> MandatoryUnits { get; set; }
        public List<BO.Unit> OptionalUnits { get; set; }
        public ComboBox[] UnitComboBoxes = new ComboBox[18];
        public ComboBox[] GradeComboBoxes = new ComboBox[18];

        public MainWindow()
        {
            MandatoryUnits = new List<BO.Unit>();
            OptionalUnits = new List<BO.Unit>();
            
            BO.Unit[] units = new XmlUnitInformation().Load();

            foreach (BO.Unit unit in units)
            {
                if (unit.IsMandatory)
                    MandatoryUnits.Add(unit);
                else
                    OptionalUnits.Add(unit);
            }

            InitializeComponent();
        }
        

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            #region Add ComboBox's to array
            
            UnitComboBoxes[0] = cbUnit1;
            UnitComboBoxes[1] = cbUnit2;
            UnitComboBoxes[2] = cbUnit3;
            UnitComboBoxes[3] = cbUnit4;
            UnitComboBoxes[4] = cbUnit5;
            UnitComboBoxes[5] = cbUnit6;
            UnitComboBoxes[6] = cbUnit7;
            UnitComboBoxes[7] = cbUnit8;
            UnitComboBoxes[8] = cbUnit9;
            UnitComboBoxes[9] = cbUnit10;
            UnitComboBoxes[10] = cbUnit11;
            UnitComboBoxes[11] = cbUnit12;
            UnitComboBoxes[12] = cbUnit13;
            UnitComboBoxes[13] = cbUnit14;
            UnitComboBoxes[14] = cbUnit15;
            UnitComboBoxes[15] = cbUnit16;
            UnitComboBoxes[16] = cbUnit17;
            UnitComboBoxes[17] = cbUnit18;

            GradeComboBoxes[0] = cbGrade1;
            GradeComboBoxes[1] = cbGrade2;
            GradeComboBoxes[2] = cbGrade3;
            GradeComboBoxes[3] = cbGrade4;
            GradeComboBoxes[4] = cbGrade5;
            GradeComboBoxes[5] = cbGrade6;
            GradeComboBoxes[6] = cbGrade7;
            GradeComboBoxes[7] = cbGrade8;
            GradeComboBoxes[8] = cbGrade9;
            GradeComboBoxes[9] = cbGrade10;
            GradeComboBoxes[10] = cbGrade11;
            GradeComboBoxes[11] = cbGrade12;
            GradeComboBoxes[12] = cbGrade13;
            GradeComboBoxes[13] = cbGrade14;
            GradeComboBoxes[14] = cbGrade15;
            GradeComboBoxes[15] = cbGrade16;
            GradeComboBoxes[16] = cbGrade17;
            GradeComboBoxes[17] = cbGrade18;

            #endregion
            int lastMandatoryUnitIndex = 0;
            for (int i = 0; i < MandatoryUnits.Count; i++)
            {
                UnitComboBoxes[i].ItemsSource = MandatoryUnits;
                UnitComboBoxes[i].SelectedIndex = i;
                UnitComboBoxes[i].Foreground = Brushes.Red;
                UnitComboBoxes[i].IsEnabled = false;
                lastMandatoryUnitIndex = i;
            }

            for (int i = lastMandatoryUnitIndex + 1; i < UnitComboBoxes.Count(); i++)
            {
                UnitComboBoxes[i].ItemsSource = OptionalUnits;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            List<BO.Grade> grades = new List<BO.Grade>();

            try
            {
                for (int i = 0; i < UnitComboBoxes.Count(); i++)
                {
                    if (UnitComboBoxes[i].SelectedItem != null 
                        && GradeComboBoxes[i].Text != null)
                    {
                        BO.Grade grade = new BO.Grade(
                            ((BO.Unit)UnitComboBoxes[i].SelectedItem).Number,
                            ((BO.Unit)UnitComboBoxes[i].SelectedItem).Name,
                            GradeComboBoxes[i].Text);

                        grades.Add(grade);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Select something for each dropdown.");
                return;
            }

            SaveFileDialog dlgSave = new SaveFileDialog();
            dlgSave.Filter = "Grade Calculator XML | *.gcxml";
            dlgSave.ShowDialog();
            
            XmlGradeInformation xGrade = new XmlGradeInformation();
            xGrade.Path = dlgSave.FileName;
            xGrade.Save(grades.ToArray());
        }

        private void btnCalculate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Calculator calc = new Calculator();
                MessageBox.Show(calc.Calculate(GradeComboBoxes));
            }
            catch
            {
                MessageBox.Show("Make sure you have selected something for every dropdown.");
            }
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlgOpen = new OpenFileDialog();
            dlgOpen.Filter = "Grade Calculator XML | *.gcxml";
            dlgOpen.ShowDialog();

            XmlGradeInformation xmlGrades = new XmlGradeInformation();
            xmlGrades.Path = dlgOpen.FileName;
            xmlGrades.Load(
                UnitComboBoxes, 
                GradeComboBoxes,
                MandatoryUnits, 
                OptionalUnits);
        }
        
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
