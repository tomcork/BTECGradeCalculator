﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GradeCalculator
{
    class Calculator
    {
        private int PassCount;
        private int MeritCount;
        private int DistinctionCount;

        private int PassPointNumber = 7;
        private int MeritPointNumber = 8;
        private int DistinctionPointNumber = 9;

        private int CreditsPerUnit = 10;

        public Calculator()
        {
            
        }

        public string Calculate(ComboBox[] gradeComboBoxes)
        {
            string result = "";
            foreach (ComboBox comboBox in gradeComboBoxes)
            {
                if (comboBox.Text == "Pass")
                    PassCount++;
                else if (comboBox.Text == "Merit")
                    MeritCount++;
                else if (comboBox.Text == "Distinction")
                    DistinctionCount++;
            }

            int points = 
                ((PassCount * PassPointNumber) +
                 (MeritCount * MeritPointNumber) + 
                 (DistinctionCount * DistinctionPointNumber))
                 * CreditsPerUnit;

            result = getGrade(points);

            foreach (ComboBox comboBox in gradeComboBoxes)
            {
                if (comboBox.Text == "" || comboBox.Text == null)
                    result = "Failed";
            }

            return result;
        }

        private string getGrade(int points)
        {
            string result = "";

            if (points < 1590)
                result = "D*D*D";

            if (points < 1560)
                result = "D*DD";

            if (points < 1530)
                result = "DDD";

            if (points < 1500)
                result = "DDM";

            if (points < 1460)
                result = "DMM";

            if (points < 1420)
                result = "MMM";

            if (points < 1380)
                result = "MMP";

            if (points < 1340)
                result = "MPP";

            if (points < 1300)
                result = "PPP";

            if (points < 1260)
                result = "Failed.";

            if (points >= 1590)
                result = "D*D*D*";

            return result;
        }
    }
}
